package net.danielbulanda.chessknightproblem.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import net.danielbulanda.chessknightproblem.logic.Board;
import net.danielbulanda.chessknightproblem.logic.Knight;
import net.danielbulanda.chessknightproblem.logic.MainLogic;

public class MainUI extends Application {
    private StackPane pane;
    private HBox topBar;
    private Label widthLabel;
    private TextField widthInput;
    private Label heightLabel;
    private TextField heightInput;
    private Button openPathButton;
    private Button closedPathButton;
    private GridPane chessBoard;
    private VBox vbox;
    private Label noteLabel;
    private Button restartButton;

    @Override
    public void start(Stage stage) throws Exception {
        pane = new StackPane();
        pane.setPrefSize(900, 935);

        topBar = new HBox(8);
        topBar.setBackground(new Background(new BackgroundFill(Color.web("#88aaee"), CornerRadii.EMPTY, Insets.EMPTY)));
        topBar.setAlignment(Pos.CENTER_LEFT);

        widthLabel = new Label("Board width:");
        widthInput = new TextField();
        widthInput.setPrefWidth(50);
        widthInput.setText("8");

        heightLabel = new Label("height:");
        heightInput = new TextField();
        heightInput.setPrefWidth(50);
        heightInput.setText("8");

        openPathButton = new Button("Create open path");
        openPathButton.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"),
                new CornerRadii(2), Insets.EMPTY)));
        closedPathButton = new Button("Create closed path");
        closedPathButton.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"),
                new CornerRadii(2), Insets.EMPTY)));

        noteLabel = new Label("");
        noteLabel.setTextFill(Color.web("#bb3333"));

        restartButton = new Button("Try Again");
        restartButton.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"),
                new CornerRadii(2), Insets.EMPTY)));
        restartButton.setVisible(false);

        openPathButton.setOnAction((event) -> {
            createBoard(Integer.valueOf(widthInput.getText()), Integer.valueOf(heightInput.getText()), false);

            openPathButton.setBackground(new Background(new BackgroundFill(Color.web("#bbeebb"),
                    new CornerRadii(2), Insets.EMPTY)));
            openPathButton.setOnAction((e) -> {});
            closedPathButton.setDisable(true);
            widthInput.setDisable(true);
            heightInput.setDisable(true);
            restartButton.setVisible(true);
        });
        closedPathButton.setOnAction((event) -> {
            createBoard(Integer.valueOf(widthInput.getText()), Integer.valueOf(heightInput.getText()), true);

            closedPathButton.setBackground(new Background(new BackgroundFill(Color.web("#bbeebb"),
                    new CornerRadii(2), Insets.EMPTY)));
            closedPathButton.setOnAction((e) -> {});
            openPathButton.setDisable(true);
            widthInput.setDisable(true);
            heightInput.setDisable(true);
            restartButton.setVisible(true);
        });

        restartButton.setOnAction((event) -> {
            stage.close();
            Platform.runLater( () -> {
                try {
                    new MainUI().start( new Stage() );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });

        topBar.getChildren().addAll(widthLabel, widthInput, heightLabel, heightInput, openPathButton,
                closedPathButton, noteLabel, restartButton);
        topBar.setPadding(new Insets(5, 5, 5, 5));

        chessBoard = new GridPane();

        vbox = new VBox(0);
        vbox.getChildren().addAll(topBar, chessBoard);

        pane.getChildren().add(vbox);

        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();

        chessBoard.setPrefHeight(4000);
    }

    void createBoard(int width, int height, boolean closed) {
        MainLogic mainLogic = new MainLogic(width, height);
        Board board = mainLogic.findPath(closed);
        Knight knight = mainLogic.getKnight();

        if (!board.isTourComplete()) {
            noteLabel.setText("Unable to find complete tour for given board size!");
        } else if (!knight.isPathClosed(board) && closed) {
            noteLabel.setText("Unable to find complete closed path for given board size!");
        } else {
            noteLabel.setText("");
        }

        for (int row = 0; row < width; row++) {
            for (int col = 0; col < height; col ++) {
                StackPane square = new StackPane();
                String color ;
                if ((row + col) % 2 == 0) {
                    color = "white";
                } else {
                    color = "grey";
                }

                if (board.getVisitsOrder(row, col) == 1 || board.getVisitsOrder(row, col) == width * height) {
                    color = "#bbeebb";
                }
                square.setStyle("-fx-background-color: "+color+";");
                chessBoard.add(square, col, row);

                Label order = new Label(String.valueOf(board.getVisitsOrder(row, col)));
                square.getChildren().addAll(order);
            }
        }

        for (int i = 0; i < width; i++) {
            chessBoard.getRowConstraints().add(new RowConstraints(5, Control.USE_COMPUTED_SIZE,
                    Double.POSITIVE_INFINITY, Priority.ALWAYS, VPos.CENTER, true));
        }
        for (int i = 0; i < height; i++) {
            chessBoard.getColumnConstraints().add(new ColumnConstraints(5, Control.USE_COMPUTED_SIZE,
                    Double.POSITIVE_INFINITY, Priority.ALWAYS, HPos.CENTER, true));
        }
        System.out.println(board);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
