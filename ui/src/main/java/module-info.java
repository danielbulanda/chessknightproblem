module ui {
    requires logic;
    requires javafx.controls;
    opens net.danielbulanda.chessknightproblem.ui to javafx.graphics;
}